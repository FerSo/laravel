
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="/home">
                    <i class="icon-dashboard"></i>
                    <span>Principal</span>
                </a>
            </li>
            <li>
                <a href="{{route('nuevo.producto')}}">
                    <i class="icon-coffee"></i>
                    <span>Nuevo Producto</span>
                </a>
            </li>
            <li>
                <a href="{{route('productos.todos')}}">
                    <i class="icon-list"></i>
                    <span>Todos los Productos</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
